import express from "express";
import { customerApi } from "./api/customer.api";
import { stockApi } from "./api/stock.api";
import { rentalApi } from "./api/rental.api";

const PORT: number = 4242;

const app = express();
app.use(express.json());
app.use("/api/stock", stockApi);
app.use("/api/rental", rentalApi);
app.use("/api/customer", customerApi);
app.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
});
