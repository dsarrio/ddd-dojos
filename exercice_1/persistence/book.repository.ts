import { Book } from "../domain/book.domain";


class BookRepository {

    private books: Map<String, Book> = new Map();

    save(book: Book) {
        this.books.set(book._id, book);
    }

    delete(book: Book) {
        this.books.delete(book._id);
    }

    findById(id: string): Book | undefined {
        return this.books.get(id);
    }

    findByTitle(title: string): Book[] {
        var matches: Book[] = [];
        for (let book of this.books.values()) {
            if (book.title.includes(title)) {
                matches.push(book);
                continue;
            }
        }
        return matches;
    }

    findByPriceRange(min: number, max: number): Book[] {
        if (min < 0 || max > 0 || min > max) {
            throw new Error('Invalid range');
        }

        var matches: Book[] = [];
        for (let book of this.books.values()) {
            if (book.rentalPrice >= min && book.rentalPrice <= max) {
                matches.push(book);
                continue;
            }
        }
        return matches;
    }
}

export const bookRepository = new BookRepository();
