import moment from "moment";
import { Rental } from "../domain/rental.domain";


class RentalRepository {

    private rentals: Map<String, Rental> = new Map();

    save(rental: Rental) {
        this.rentals.set(rental._id, rental);
    }

    delete(rental: Rental) {
        this.rentals.delete(rental._id);
    }

    findById(id: string): Rental | undefined {
        return this.rentals.get(id);
    }

    findByCustomerId(customerId: string): Rental[] {
        var matches: Rental[] = [];
        for (let rental of this.rentals.values()) {
            if (rental.customerId === customerId) {
                matches.push(rental);
                continue;
            }
        }
        return matches;
    }

    findByBookId(bookId: string): Rental[] {
        var matches: Rental[] = [];
        for (let rental of this.rentals.values()) {
            if (rental.bookId === bookId) {
                matches.push(rental);
                continue;
            }
        }
        return matches;
    }

    findByRentDateBefore(ref: moment.Moment): Rental[] {
        var matches: Rental[] = [];
        for (let rental of this.rentals.values()) {
            if (moment(rental.rentDate).isBefore(ref)) {
                matches.push(rental);
                continue;
            }
        }
        return matches;
    }
}

export const rentalRepository = new RentalRepository();
