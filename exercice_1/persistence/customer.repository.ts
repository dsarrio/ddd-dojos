import { Customer } from "../domain/customer.domain";


class CustomerRepository {

    private customers: Map<String, Customer> = new Map();

    save(customer: Customer) {
        this.customers.set(customer._id, customer);
    }

    findById(id: string): Customer | undefined {
        return this.customers.get(id);
    }
}

export const customerRepository = new CustomerRepository();
