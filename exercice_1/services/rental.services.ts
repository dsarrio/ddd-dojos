import moment from "moment";
import { Rental } from "../domain/rental.domain";
import { bookRepository } from "../persistence/book.repository";
import { customerRepository } from "../persistence/customer.repository";
import { rentalRepository } from "../persistence/rental.repository";

function sendEmail(to: string, from: string, subject: string, content: string) { /* call the fatty mailing service provider */ }
function sendSMS(phone: string, content: string) { /* call the fatty sms service provider */ }

function sendReminder(rental: Rental) {
    const customer = customerRepository.findById(rental.customerId)!;
    const book = bookRepository.findById(rental.bookId)!;
    const now = moment();
    const duration = moment.duration(now.diff(rental.rentDate));
    var currentCost = duration.asDays() * book.rentalPrice;

    if (customer.email !== undefined && customer.email.match(/.*@.*\..*/)) {
        sendEmail(customer.email, "no-reply@domain.tld", "You should start thinking to bring back my book !", "Current cost is " + currentCost + "€...");
    }
}

function sendInjuction(rental: Rental) {
    const customer = customerRepository.findById(rental.customerId)!;
    const book = bookRepository.findById(rental.bookId)!;
    const now = moment();
    const duration = moment.duration(now.diff(rental.rentDate));
    var currentCost = Math.max(8, duration.asDays()) * book.rentalPrice + Math.max(0, duration.asDays() - 8) * book.rentalPrice * 2;

    if (customer.phoneNumber !== undefined && customer.phoneNumber.match(/+?[0-9]+/)) {
        var phone = customer.phoneNumber;
        if (!phone.startsWith('+33')) {
            phone = "+33" + phone;
        }
        sendSMS(phone, "Bring back my book you son of a b****, that wil cost you " + currentCost + "€ !");
    }
}

export function sendReminders() {
    const remindLimit = moment().subtract(5, "days");
    const dangerLimit = moment().subtract(7, "days");

    const rentals = rentalRepository.findByRentDateBefore(remindLimit);
    for (const rental of rentals) {
        if (rental.rentDate.isBefore(dangerLimit)) {
            sendInjuction(rental);
        } else {
            sendReminder(rental)
        }
    }
}
