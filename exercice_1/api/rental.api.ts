import { randomUUID } from "crypto";
import express, { Request, Response } from "express";
import moment from "moment";
import { v4 as uuidv4 } from "uuid";
import { Rental } from "../domain/rental.domain";
import { bookRepository } from "../persistence/book.repository";
import { customerRepository } from "../persistence/customer.repository";
import { rentalRepository } from "../persistence/rental.repository";
import { sendReminders } from "../services/rental.services";

export const rentalApi = express.Router();

rentalApi.post("/", async (req: Request, res: Response) => {
    const bookId = req.body.bookId;
    const customerId = req.body.customerId;

    const book = bookRepository.findById(bookId);
    if (book === undefined) {
        return res.status(404).send("Unknown book");
    }

    const customer = customerRepository.findById(customerId);
    if (customer === undefined) {
        return res.status(404).send("Unknown customer");
    }

    const currRent = rentalRepository.findByBookId(bookId);
    if (currRent !== undefined) {
        return res.status(400).send("book already rent");
    }

    const rental: Rental = {
        _id: uuidv4(),
        customerId: customerId,
        bookId: bookId,
        rentDate: moment(),
    }

    rentalRepository.save(rental);

    return res.status(200).send(rental);
});

rentalApi.get("/:id", async (req: Request, res: Response) => {
    const rental = rentalRepository.findById(req.params.id);
    if (rental === undefined) {
        return res.status(404).send("rental not found");
    }

    const customer = customerRepository.findById(rental.customerId);
    const book = bookRepository.findById(rental.bookId)!;
    const now = moment();
    const duration = moment.duration(now.diff(rental.rentDate));
    var currentCost = duration.asDays() * book.rentalPrice;

    return res.status(200).send({
        id: rental._id,
        book: book,
        customer: customer,
        currentCost,
    })
});

rentalApi.delete("/:id", async (req: Request, res: Response) => {
    const rental = rentalRepository.findById(req.params.id);
    if (rental === undefined) {
        return res.status(404).send("rental not found");
    }

    rentalRepository.delete(rental);

    return res.sendStatus(200);
});

rentalApi.post("/send-reminders", async (req: Request, res: Response) => {
    sendReminders();
    return res.sendStatus(200);
});
