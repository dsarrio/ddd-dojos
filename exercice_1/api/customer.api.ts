import express, { Request, Response } from "express";
import { v4 as uuidv4 } from "uuid";
import { Customer } from "../domain/customer.domain";
import { customerRepository } from "../persistence/customer.repository";

export const customerApi = express.Router();

customerApi.post("/", async (req: Request, res: Response) => {
    const customer: Customer = {
        _id: uuidv4(),
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        address: req.body.address,
        zipcode: req.body.zipcode,
        phoneNumber: req.body.phoneNumber,
        email: req.body.email,
    };

    let err = "";
    if (!customer.firstName || customer.firstName.length == 0) err += "firstName must be set";
    if (!customer.lastName || customer.lastName.length == 0) err += "lastName must be set";
    if (!customer.address || customer.address.length == 0) err += "address must be set";
    if (!customer.zipcode || customer.zipcode.length == 0) err += "zipcode must be set";
    if (!customer.phoneNumber || customer.phoneNumber.length == 0) err += "phoneNumber must be set";
    if (!customer.email || customer.email.length == 0) err += "email must be set";

    if (err) return res.status(400).send(err);

    customerRepository.save(customer);

    return res.status(200).send(customer);
});
