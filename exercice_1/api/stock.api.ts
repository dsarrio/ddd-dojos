import { randomUUID } from "crypto";
import express, { Request, Response } from "express";
import { v4 as uuidv4 } from "uuid";
import { Book } from "../domain/book.domain";
import { bookRepository } from "../persistence/book.repository";
import { rentalRepository } from "../persistence/rental.repository";

export const stockApi = express.Router();

stockApi.post("/", async (req: Request, res: Response) => {
    const book: Book = {
        _id: uuidv4(),
        title: req.body.title,
        authorFirstname: req.body.authorFirstname,
        authorLastname: req.body.authorLastname,
        isbn: req.body.isbn,
        language: req.body.language,
        nbPages: req.body.nbPages,
        format: req.body.format,
        rentalPrice: req.body.rentalPrice,
        isSellable: req.body.isSellable,
        sellPrice: req.body.sellPrice,
    };

    let err = [];
    if (!book.title || book.title.length == 0) err.push("title must be set");
    if (!book.authorFirstname || book.authorFirstname.length == 0) err.push("authorFirstname must be set");
    if (!book.authorLastname || book.authorLastname.length == 0) err.push("authorFirstname must be set");
    if (!book.isbn || book.isbn.length == 0) err.push("isbn must be set");
    if (!book.language || book.language.length == 0) err.push("language must be set");
    if (!book.nbPages || book.nbPages <= 0) err.push("language must be set and greater than 0");
    if (!book.format || book.format.length <= 0) err.push("format must be set");
    if (book.isSellable === undefined) err.push("isSellable must be set");
    if (book.isSellable) {
        if (!(book.sellPrice >= 0)) err.push("sellPrice must be set and greater than 0");
    } else {
        if (!(book.rentalPrice >= 0)) err.push("rentalPrice must be set and greater than 0");
    }

    const parts = book.isbn.split('-');
    const digits = parts[0].split('');
    const controlKey = parseInt(parts[1]);
    if (digits.length != 9) throw Error('invalid isbn number');
    if (controlKey < 0 || controlKey > 9) throw Error('invalid isbn number');
    var crc = 0;
    for (var i = 10; i > 1; i--) {
        crc += parseInt(digits[10 - i]) * i;
    }
    crc = 11 - crc % 11;
    if (crc != controlKey) err.push("Invalid isbn");

    if (book.language.toLowerCase() == "en" || book.language.toLowerCase() == "fr" || book.language.toLowerCase() == "es") {
        err.push("Unsupported language");
    } 

    if (!(book.format in ["POCKET_110x180", "DIGEST_140x216", "ROMAN_A5_150x210", "ROYAL_160x240", "A4_210x297"])) {
        err.push("Unknown format");
    }

    if (err) return res.status(400).send({error: err});

    bookRepository.save(book);

    return res.status(200).send(book);
});

stockApi.delete("/:id", async (req: Request, res: Response) => {
    const bookId = req.params.id;
    const book = bookRepository.findById(bookId)
    if (book === undefined) {
        return res.sendStatus(404);
    }

    if (book.isSellable) {
        return res.status(400).send("book is for sale");
    } else {
        const currRent = rentalRepository.findByBookId(bookId);
        if (currRent !== undefined) {
            return res.status(400).send({error: "Book is currently rent"});
        }
    }

    bookRepository.delete(book);

    return res.sendStatus(200);
});
