
export interface Customer {
    _id: string

    firstName: string
    lastName: string
    address: string
    zipcode: string
    phoneNumber: string
    email: string
}
