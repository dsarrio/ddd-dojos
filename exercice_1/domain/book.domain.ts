
export interface Book {
    _id: string

    title: string
    authorFirstname: string
    authorLastname: string
    isbn: string
    language: string
    nbPages: number
    format: string

    rentalPrice: number

    isSellable: boolean
    sellPrice: number
}
