import moment from "moment";

export interface Rental {
    _id: string

    customerId: string
    bookId: string
    rentDate: moment.Moment
}
