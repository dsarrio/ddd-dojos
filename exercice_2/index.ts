
// Convert 61.8 miles to meters
(() => {
    var meters = 61.8 * 1609.0;
    console.log(meters);
})();

// Convert 43.45 nautic miles to meters
(() => {
    var meters = 43.45 * 1852.0;
    console.log(meters);
})();

// Convert 6 hours to seconds
(() => {
    var seconds = 6 * 60 * 60;
    console.log(seconds);
})();

// Compute what was my speed when i drove 111.8 miles over 3 hours (speed in mi/h)
(() => {
    var distancePerHour = 111.8  / 3.0;
    console.log(distancePerHour, "mi/h");
})();

// Compute what was my overall speed when i did 2456.5 yards during 6 hours and 600 kilometers in 8 hours and 42 minutes ( speed in m/s )
(() => {
    var metersA = 2456.5 * 0.9144;
    var durationA = 6 * 60 * 60;
    var metersB = 600 * 1000;
    var durationB = 8 * 60 * 60 + 42 * 60;
    var distanceAB = metersA + metersB;
    var durationAB = durationA + durationB;
    console.log(distanceAB / durationAB, "m/s");
})();
